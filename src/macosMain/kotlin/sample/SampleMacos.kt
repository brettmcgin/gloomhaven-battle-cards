package sample

data class BattleCard(
        val name: String,
        val description: String,
        val checkmarks: Int
)

val battleCards = arrayListOf(
        BattleCard("Aggressor", "Have one or more monsters present on the map at the beginning of every round during the scenario.", 2),
        BattleCard("Diehard", "Never allow your current hit point value to drop below half your maximum hit point value (rounded up)", 1),
        BattleCard("Dynamo", "Kill a monster during the scenario by causing at least 4 more points of damage to it than necessary.", 1),
        BattleCard("Executioner", "Kill an undamaged monster with a single attack during the scenario.", 1),
        BattleCard("Explorer", "Reveal a room tile by opening a door on your turn during the scenario.", 1),
        BattleCard("Fast Healer", "End the scenario with hit points equal to your maximum hit point value.", 1),
        BattleCard("Hoarder", "Loot five or more money tokens during the scenario.", 1),
        BattleCard("Hunter", "Kill one or more elite monsters during the scenario.", 1),
        BattleCard("Indigent", "Loot no money tokens or treasure overlay tiles during this scenario.", 2),
        BattleCard("Layabout", "Gain 7 or fewer experience points during the scenario.", 2),
        BattleCard("Masochist", "End the scenario with 3 or fewer hit points.", 1),
        BattleCard("Neutralizer", "Cause a trap to be sprung or disarmed on your turn or on the turn of one of your summons during the scenario.", 1),
        BattleCard("Opener", "Kill the first monster to die in the scenario.", 1),
        BattleCard("Pacifist", "Kill three or fewer monsters.", 1),
        BattleCard("Plunderer", "Loot a treasure overlay tile during the scenario.", 1),
        BattleCard("Professional", "Use your equipped items a number of times equal to or greater than (your level + 2) during the scenario.", 1),
        BattleCard("Protector", "Allow none of your character allies to become exhausted during the scenario.", 1),
        BattleCard("Purist", "Use no items during the scenario.", 2),
        BattleCard("Sadist", "Kill five or more monsters.", 1),
        BattleCard("Scrambler", "Never long rest.", 1),
        BattleCard("Straggler", "Never short rest.", 1),
        BattleCard("Streamliner", "Have five or more total cards in your hand and discard at the end of the Scenario.", 1),
        BattleCard("Workhorse", "Gain 13 or more Experience points during the Scenario.", 1),
        BattleCard("Zealot", "Have three or fewer total cards in your hand and discard at the end of the scenario.", 1)
)

fun main() {
    (1..8).forEach { i ->
        battleCards.shuffle()
        val battleCard = battleCards[0]
        println("Name: ${battleCard.name}, ${"✔️".repeat(battleCard.checkmarks)}")
        println(battleCard.description)
        battleCards.removeAt(0)
        if (i % 2 == 0) println()
    }
}